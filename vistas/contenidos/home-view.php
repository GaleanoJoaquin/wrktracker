<?php 
    if($_SESSION['tipo_sbp']!="Administrador"){
        echo $lc->forzar_cierre_sesion_controlador();
    }
?>
<div class="container-fluid">
	<div class="page-header">
	  <h1 class="text-titles">Sistema <small>Conteo de usuarios registrados hasta la fecha</small></h1>
	</div>
</div>
<div class="full-box text-center" style="padding: 30px 10px;">
    <?php
        require "./controladores/administradorControlador.php";
        $IAdmin= new administradorControlador();
        $CAdmin=$IAdmin->datos_administrador_controlador("Conteo",0);
    ?>
	<article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Administradores
		</div>
		<div class="full-box tile-icon text-center">
			<i class="zmdi zmdi-account"></i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box"><?php echo $CAdmin->rowCount(); ?></p>
			<small>Registrados</small>
		</div>
	</article>
    
    <?php
        require "./controladores/empleadoControlador.php";
        $IEmpleado= new empleadoControlador();
        $CEmpleados=$IEmpleado->datos_empleado_controlador("Conteo",0);
    ?>
	<article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Empleados
		</div>
		<div class="full-box tile-icon text-center">
			<i class="zmdi zmdi-male-alt"></i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box"><?php echo $CEmpleados->rowCount(); ?></p>
			<small>Registrados</small>
		</div>
	</article>

</div>
<div class="container-fluid">
	<div class="page-header">
	  <h1 class="text-titles">Sistema<small> Linea de tiempo - Inicio y finalizacion de sesión de usuarios</small></h1>
	</div>
	<section id="cd-timeline" class="cd-container">
        <?php
            require "./controladores/bitacoraControlador.php";
            $IBitacora= new bitacoraControlador();

            echo $IBitacora->listado_bitacora_controlador(15);
        ?>
    </section>
</div>