<!-- Content page -->
<div class="container-fluid">
	<div class="page-header">
	  <h1 class="text-titles"><i class="zmdi zmdi-book-image zmdi-hc-fw"></i> TAREAS</h1>
	</div>
	<p class="lead">En este sector se listan las tareas asignadas. Por favor, revise el estado de sus tareas para su revisión.</p>
</div>
<div class="container-fluid text-center">
	<div class="btn-group">
      <a href="javascript:void(0)" class="btn btn-default btn-raised">SELECCIONE EL ESTADO DE LAS TAREAS ASIGNADAS</a>
      <a href="javascript:void(0)" data-target="dropdown-menu" class="btn btn-default btn-raised dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li><a href="#!">Pendientes</a></li>
        <li><a href="#!">En proceso</a></li>
        <li><a href="#!">Realizadas</a></li>
      </ul>
    </div>
</div>
<div class="container-fluid">
	<h2 class="text-titles text-center">Categoría seleccionada</h2>
	<div class="row">
		<div class="col-xs-12">
			<div class="list-group">
				<div class="list-group-item">
					<div class="row-picture">
						 
					</div>
					<div class="row-content">
						<h4 class="list-group-item-heading">1 - Título completo de la tarea</h4>
						<p class="list-group-item-text">
							<strong> </strong>Nombre de la tarea <br>
							<a href="#" class="btn btn-primary" title="Más información"><i class="zmdi zmdi-info"></i></a>
							<a href="#!" class="btn btn-primary" title="Ver Ubicacion de la tarea"><i class="zmdi zmdi-pin"></i></a>
							<a href="#!" class="btn btn-primary" title="Descargar PDF con detalle"><i class="zmdi zmdi-cloud-download"></i></a>
							<a href="#" class="btn btn-primary" title="Gestionar tarea"><i class="zmdi zmdi-wrench"></i></a>
						</p>
					</div>
				</div>
				<div class="list-group-separator"></div>
				<div class="list-group-item">
				</div>
				<div class="list-group-separator"></div>
			</div>
			<nav class="text-center">
				<ul class="pagination pagination-sm">
					<li class="disabled"><a href="javascript:void(0)">«</a></li>
					<li class="active"><a href="javascript:void(0)">1</a></li>
					<li><a href="javascript:void(0)">2</a></li>
					<li><a href="javascript:void(0)">3</a></li>
					<li><a href="javascript:void(0)">4</a></li>
					<li><a href="javascript:void(0)">5</a></li>
					<li><a href="javascript:void(0)">»</a></li>
				</ul>
			</nav>
		</div>
	</div>
</div>