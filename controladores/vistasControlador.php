<?php
	require_once "./modelos/vistasModelo.php";
	class vistasControlador extends vistasModelo{

		//Solicita las vistas al modelo vistasModelo

		public static function obtener_plantilla_controlador(){
			return require_once "./vistas/plantilla.php";
		}

		public static function obtener_vistas_controlador(){
			if(isset($_GET['views'])){
				$ruta=explode("/", $_GET['views']); //Explode divide un string en varios string
				$respuesta=vistasModelo::obtener_vistas_modelo($ruta[0]);
			}else{
				$respuesta="login";
			}
			return $respuesta; 
		}
	}