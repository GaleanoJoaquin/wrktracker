<?php
	if($peticionAjax){
		require_once "../core/configAPP.php";
	}else{
		require_once "./core/configAPP.php";
	}
	class mainModel{
		//Conexión a la base de datos
		protected static function conectar(){
			$enlace = new PDO(SGBD,USER,PASS);
			return $enlace;
		}
		//Ejecución de consulta simple, se recibe el paramatro $consulta y se crea la query
		protected static function ejecutar_consulta_simple($consulta){
			$respuesta=self::conectar()->prepare($consulta);
			$respuesta->execute();
			return $respuesta;
		}

		//Funcion para agregar cuenta. Conecta con la base da datos, prepara la query tomando como parametros los datos en enviados a traves de la variable $datos
		protected static function agregar_cuenta($datos){
			$sql=self::conectar()->prepare("INSERT INTO cuenta(CuentaCodigo,CuentaPrivilegio,CuentaUsuario,CuentaClave,CuentaEmail,CuentaEstado,CuentaTipo,CuentaGenero,CuentaFoto) VALUES(:Codigo,:Privilegio,:Usuario,:Clave,:Email,:Estado,:Tipo,:Genero,:Foto)");
			$sql->bindParam(":Codigo",$datos['Codigo']);
			$sql->bindParam(":Privilegio",$datos['Privilegio']);
			$sql->bindParam(":Usuario",$datos['Usuario']);
			$sql->bindParam(":Clave",$datos['Clave']);
			$sql->bindParam(":Email",$datos['Email']);
			$sql->bindParam(":Estado",$datos['Estado']);
			$sql->bindParam(":Tipo",$datos['Tipo']);
			$sql->bindParam(":Genero",$datos['Genero']);
			$sql->bindParam(":Foto",$datos['Foto']);
			$sql->execute();
			return $sql;
		}
		//Funcion para eliminar cuenta de usuario
		protected static function eliminar_cuenta($codigo){
			$sql=self::conectar()->prepare("DELETE FROM cuenta WHERE CuentaCodigo=:Codigo");
			$sql->bindParam(":Codigo",$codigo);
			$sql->execute();
			return $sql;
		}

		//Recibe los datos de la cuenta del usuario
		protected static function datos_cuenta($codigo,$tipo){
			$query=self::conectar()->prepare("SELECT * FROM cuenta WHERE CuentaCodigo=:Codigo AND CuentaTipo=:Tipo");
			$query->bindParam(":Codigo",$codigo);
			$query->bindParam(":Tipo",$tipo);
			$query->execute();
			return $query;
		}
		//Actualiza los datos del usuario
		protected static function actualizar_cuenta($datos){
			//Conecto a la base datos y luego preparo la query
			$query=self::conectar()->prepare("UPDATE cuenta SET CuentaPrivilegio=:Privilegio,CuentaUsuario=:Usuario,CuentaClave=:Clave,CuentaEmail=:Email,CuentaEstado=:Estado,CuentaGenero=:Genero,CuentaFoto=:Foto WHERE CuentaCodigo=:Codigo");
			$query->bindParam(":Privilegio",$datos['CuentaPrivilegio']);
			$query->bindParam(":Usuario",$datos['CuentaUsuario']);
			$query->bindParam(":Clave",$datos['CuentaClave']);
			$query->bindParam(":Email",$datos['CuentaEmail']);
			$query->bindParam(":Estado",$datos['CuentaEstado']);
			$query->bindParam(":Genero",$datos['CuentaGenero']);
			$query->bindParam(":Foto",$datos['CuentaFoto']);
			$query->bindParam(":Codigo",$datos['CuentaCodigo']);
			$query->execute();
			return $query;
		}
		//Guarda los datos de los registros de conexion del usuario, también cuando cierra session
		protected static function guardar_bitacora($datos){
			$sql=self::conectar()->prepare("INSERT INTO bitacora(BitacoraCodigo,BitacoraFecha,BitacoraHoraInicio,BitacoraHoraFinal,BitacoraTipo,BitacoraYear,CuentaCodigo) VALUES(:Codigo,:Fecha,:HoraInicio,:HoraFinal,:Tipo,:Year,:Cuenta)");
			$sql->bindParam(":Codigo",$datos['Codigo']);
			$sql->bindParam(":Fecha",$datos['Fecha']);
			$sql->bindParam(":HoraInicio",$datos['HoraInicio']);
			$sql->bindParam(":HoraFinal",$datos['HoraFinal']);
			$sql->bindParam(":Tipo",$datos['Tipo']);
			$sql->bindParam(":Year",$datos['Year']);
			$sql->bindParam(":Cuenta",$datos['Cuenta']);
			$sql->execute();
			return $sql;
		}
		//Actulización de campos de la bitacora del usuario
		protected static function actualizar_bitacora($codigo,$hora){
			$sql=self::conectar()->prepare("UPDATE bitacora SET BitacoraHoraFinal=:Hora WHERE BitacoraCodigo=:Codigo");
			$sql->bindParam(":Hora",$hora);
			$sql->bindParam(":Codigo",$codigo);
			$sql->execute();
			return $sql;
		}
		//Elimina los datos de la bitacora del usuario
		protected static function eliminar_bitacora($codigo){
			$sql=self::conectar()->prepare("DELETE FROM bitacora WHERE CuentaCodigo=:Codigo");
			$sql->bindParam(":Codigo",$codigo);
			$sql->execute();
			return $sql;
		}

		//Funcion para encriptar caracter
		public static function encryption($string){
			$output=FALSE;
			$key=hash('sha256', SECRET_KEY);
			$iv=substr(hash('sha256', SECRET_IV), 0, 16);
			$output=openssl_encrypt($string, METHOD, $key, 0, $iv);
			$output=base64_encode($output);
			return $output;
		}
		//Funcion para desencriptar caracter
		protected static function decryption($string){
			$key=hash('sha256', SECRET_KEY);
			$iv=substr(hash('sha256', SECRET_IV), 0, 16);
			$output=openssl_decrypt(base64_decode($string), METHOD, $key, 0, $iv);
			return $output;
		}

		//Genera condigo aleatorio para identar las cuentas 
		protected static function generar_codigo_aleatorio($letra,$longitud,$num){
			for($i=1; $i<=$longitud; $i++){
				$numero = rand(0,9);
				$letra.= $numero;
			}
			return $letra.$num;
		}
		//Limpa cadena de caracteres ingresados por el usuario en los campos en blanco
		protected static function limpiar_cadena($cadena){
			$cadena=trim($cadena);
			$cadena=stripslashes($cadena);
			$cadena=str_ireplace("<script>", "", $cadena);
			$cadena=str_ireplace("</script>", "", $cadena);
			$cadena=str_ireplace("<script src", "", $cadena);
			$cadena=str_ireplace("<script type=", "", $cadena);
			$cadena=str_ireplace("SELECT * FROM", "", $cadena);
			$cadena=str_ireplace("DELETE FROM", "", $cadena);
			$cadena=str_ireplace("INSERT INTO", "", $cadena);
			$cadena=str_ireplace("--", "", $cadena);
			$cadena=str_ireplace("^", "", $cadena);
			$cadena=str_ireplace("[", "", $cadena);
			$cadena=str_ireplace("]", "", $cadena);
			$cadena=str_ireplace("==", "", $cadena);
			$cadena=str_ireplace(";", "", $cadena);
			return $cadena;
		}

		//Funcion para el vio de alertas
		protected static function sweet_alert($datos){
			if($datos['Alerta']=="simple"){
				$alerta="
					<script>
						swal(
						  '".$datos['Titulo']."',
						  '".$datos['Texto']."',
						  '".$datos['Tipo']."'
						);
					</script>
				";
			}elseif($datos['Alerta']=="recargar"){
				$alerta="
					<script>
						swal({
						  title: '".$datos['Titulo']."',
						  text: '".$datos['Texto']."',
						  type: '".$datos['Tipo']."',
						  confirmButtonText: 'Aceptar'
						}).then(function () {
							location.reload();
						});
					</script>
				";
			}elseif($datos['Alerta']=="limpiar"){
				$alerta="
					<script>
						swal({
						  title: '".$datos['Titulo']."',
						  text: '".$datos['Texto']."',
						  type: '".$datos['Tipo']."',
						  confirmButtonText: 'Aceptar'
						}).then(function () {
							$('.FormularioAjax')[0].reset();
						});
					</script>
				";
			}
			return $alerta;
		}
	}