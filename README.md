# WRKtracker
 
 WRKTracker es un sistema de monitoreo online y asignación de tareas mediante el cual su empresa podrá obtener una experiencia sumamente satisfactoria a la hora de asignar tareas a sus empleados, controlar su cumplimiento y obtener reportes de cómo evolucionan y el desempeño y cumplimiento de las personas que las realizan.
El objetivo de nuestro sistema comprende resolver problemas de transparencia entre empleados y el empleador, ya que ambos podrán tener registros que avalen a ambas partes cuando de asignar tareas se trate.
El administrador designado de su empresa tendrá la posibilidad de crear usuarios (empleados) con sus respectivas credenciales, las que les permitirán a los mismos iniciar sesión en el sitio y obtener en su perfil de manera explícita la tarea que deben realizar e interiorizarse sobre la misma.
Al momento de asignar una tarea el empleador podrá detallar la ubicación en la cual hay que resolverla y este lugar es a donde se deberá dirigir el empleado. 
Una vez estando en el lugar indicado en el sistema, el empleado iniciará sesión desde el navegador web de su celular para registrar la posición en la que se encuentra y de esta manera se dará por comenzado el inicio del horario del trabajo asignado. Luego de terminada la tarea el empleado deberá registrar mediante la web que ya culminó con el trabajo y subirá una foto de la tarea cumplida para poder ser corroborada.
Ofrecemos una interfaz de usuario amigable para poder obtener la mejor experiencia en el sitio. 

