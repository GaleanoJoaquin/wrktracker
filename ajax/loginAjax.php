<?php
	$peticionAjax=true; //Preguntamos si la peticion Ajax en verdadero o falsa 

	//recibimos los datos que vamos a enviar con Ajax
	require_once "../core/configGeneral.php"; //incluimos las config generales
	if(isset($_GET['Token'])){ //Al hacer click en el boton salir enviamos el token 
		require_once "../controladores/loginControlador.php";
		$logout= new loginControlador();
		echo $logout->cerrar_sesion_controlador(); //Si no se reciben datos se cierra la sesion
	}else{
		session_start(['name'=>'SBP']);// Si se reciben se inicia la sesion
		session_destroy();
		echo '<script> window.location.href="'.SERVERURL.'login/" </script>';
	}