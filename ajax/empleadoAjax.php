<?php
	$peticionAjax=true;
	require_once "../core/configGeneral.php";
	if(isset($_POST['dni-reg']) || isset($_POST['codigo-del']) || isset($_POST['cuenta-up'])){

		require_once "../controladores/empleadoControlador.php";
		$InsEmpleado= new empleadoControlador();

		if(isset($_POST['dni-reg']) && isset($_POST['nombre-reg']) && isset($_POST['apellido-reg'])){
				echo $InsEmpleado->agregar_empleado_controlador();
		}

		if(isset($_POST['codigo-del']) && isset($_POST['privilegio-admin'])){
			echo $InsEmpleado->eliminar_empleado_controlador();
		}

		if(isset($_POST['cuenta-up']) && isset($_POST['dni-up'])){
			echo $InsEmpleado->actualizar_empleado_controlador();
		}
		
		
	}else{
		session_start(['name'=>'SBP']);
		session_destroy();
		echo '<script> window.location.href="'.SERVERURL.'login/" </script>';
	}