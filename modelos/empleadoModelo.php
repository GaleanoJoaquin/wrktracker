<?php
	if($peticionAjax){
		require_once "../core/mainModel.php";
	}else{
		require_once "./core/mainModel.php";
	}

	class empleadoModelo extends mainModel{

		protected static function agregar_empleado_modelo($datos){
			$sql=mainModel::conectar()->prepare("INSERT INTO empleado(EmpleadoDNI,EmpleadoNombre,EmpleadoApellido,EmpleadoTelefono,EmpleadoOcupacion,EmpleadoDireccion,CuentaCodigo) VALUES(:DNI,:Nombre,:Apellido,:Telefono,:Ocupacion,:Direccion,:Codigo)");
			$sql->bindParam(":DNI",$datos['DNI']);
			$sql->bindParam(":Nombre",$datos['Nombre']);
			$sql->bindParam(":Apellido",$datos['Apellido']);
			$sql->bindParam(":Telefono",$datos['Telefono']);
			$sql->bindParam(":Ocupacion",$datos['Ocupacion']);
			$sql->bindParam(":Direccion",$datos['Direccion']);
			$sql->bindParam(":Codigo",$datos['Codigo']);
			$sql->execute();
			return $sql;
		}


		protected static function datos_empleado_modelo($tipo,$codigo){
			if($tipo=="Unico"){
				$query=mainModel::conectar()->prepare("SELECT * FROM empleado WHERE CuentaCodigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT id FROM empleado");
			}
			$query->execute();
			return $query;
		}


		protected static function eliminar_empleado_modelo($codigo){
			$query=mainModel::conectar()->prepare("DELETE FROM empleado WHERE CuentaCodigo=:Codigo");
			$query->bindParam(":Codigo",$codigo);
			$query->execute();
			return $query;
		}


		protected static function actualizar_empleado_modelo($datos){
			$query=mainModel::conectar()->prepare("UPDATE empleado SET EmpleadoDNI=:DNI,EmpleadoNombre=:Nombre,EmpleadoApellido=:Apellido,EmpleadoTelefono=:Telefono,EmpleadoOcupacion=:Ocupacion,EmpleadoDireccion=:Direccion WHERE CuentaCodigo=:Codigo");
			$query->bindParam(":DNI",$datos['DNI']);
			$query->bindParam(":Nombre",$datos['Nombre']);
			$query->bindParam(":Apellido",$datos['Apellido']);
			$query->bindParam(":Telefono",$datos['Telefono']);
			$query->bindParam(":Ocupacion",$datos['Ocupacion']);
			$query->bindParam(":Direccion",$datos['Direccion']);
			$query->bindParam(":Codigo",$datos['Codigo']);
			$query->execute();
			return $query;
		}

	}