<?php 
	class vistasModelo{
		//Funcion para obtener las vistas del sitio.
		protected static function obtener_vistas_modelo($vistas){
			$listaBlanca=["adminlist","adminsearch","admin","asigntarea","tareas","empleado","empleadolist","empleadosearch","home","myaccount","mydata","search"];
			if(in_array($vistas, $listaBlanca)){ //Comprueba si un valor existe en un array
				if(is_file("./vistas/contenidos/".$vistas."-view.php")){ // Indica si el nombre de fichero es un fichero normal
					$contenido="./vistas/contenidos/".$vistas."-view.php";
				}else{
					$contenido="login";
				}
			}elseif($vistas=="login"){
				$contenido="login";
			}elseif($vistas=="index"){
				$contenido="login";
			}else{
				$contenido="404";
			}
			return $contenido;
		}
	}
